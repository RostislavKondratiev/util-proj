import { Directive, ElementRef, HostBinding, Input, Optional, Self } from '@angular/core';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { FormFieldControl } from './form-field-control';
import { BaseInput } from './base-input';

@Directive({
  selector: 'input[appInput], textarea[appInput]',
  providers: [{provide: FormFieldControl, useExisting: AppInputDirective}],
})
export class AppInputDirective extends BaseInput {
  @HostBinding('disabled')
  protected _disabled = false;

  @HostBinding('placeholder')
  protected _placeholder = '';

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(value: any) {
    this._disabled = !!value;
  }

  @Input()
  get placeholder() {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
  }
}
