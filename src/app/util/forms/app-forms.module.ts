import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppInputDirective } from './app-input.directive';
import { AppLabelDirective } from './app-label.directive';
import { InputContainerComponent } from './input-container/input-container.component';
import { ErrorDirective } from './errors/error.directive';
import { ErrorsDirective } from './errors/errors.directive';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CheckboxDirective } from './checkbox/checkbox.directive';

const DIRECTIVES = [
  AppInputDirective,
  CheckboxDirective,
  AppLabelDirective,
  ErrorsDirective,
  ErrorDirective
];

const COMPONENTS = [
  InputContainerComponent,
  CheckboxComponent
];
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    ...COMPONENTS,
    ...DIRECTIVES
  ],
  exports: [
    ...COMPONENTS,
    ...DIRECTIVES
  ]
})
export class AppFormsModule {
}
