import {
  AfterContentInit, Component, ContentChild, ElementRef, Input, Renderer2, ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormControl, FormGroupDirective } from '@angular/forms';
import { AutoUnsubscribe } from '../../helpers/decorators';
import { FormFieldControl } from '../form-field-control';

import 'rxjs/add/operator/startWith';
import { ErrorsDirective } from '../errors/errors.directive';

@Component({
  selector: 'app-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.scss'],
  encapsulation: ViewEncapsulation.None
})
@AutoUnsubscribe()
export class InputContainerComponent implements AfterContentInit {
  @ContentChild(FormFieldControl) public input;
  @ViewChild(ErrorsDirective) public errorsDirective: ErrorsDirective;
  public control;
  @Input() public validationMark = true;

  private parent: FormGroupDirective;
  private elem: ElementRef;

  constructor(private renderer: Renderer2) {
  }

  public ngAfterContentInit() {
    this.control = this.input.control.control;
    this.parent = this.input.parent;
    this.elem = this.input.element;
    console.log(this.input);
    this.input.parent.ngSubmit.subscribe((val) => {
      this.checkFormValidity();
    });
  }

  public checkFormValidity() {
    if (this.parent.invalid) {
      this.control.updateValueAndValidity();
    }
  }

  public hasError($event) {
    if (this.validationMark) {
      if ($event) {
        this.renderer.addClass(this.elem, 'has-error');
        this.renderer.removeClass(this.elem, 'valid-field');
      } else {
        this.renderer.removeClass(this.elem, 'has-error');
        this.renderer.addClass(this.elem, 'valid-field');
      }
    }
  }
}
