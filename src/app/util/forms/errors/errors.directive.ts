import { Directive, Input, OnChanges, OnDestroy, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { FormGroupDirective, AbstractControl, FormControl } from '@angular/forms';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ErrorDetails } from './errors.interface';
import { AutoUnsubscribe } from '../../helpers/decorators';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/pluck';

@AutoUnsubscribe()
@Directive({
  selector: '[appErrors]',
  exportAs: 'appErrors'
})
export class ErrorsDirective implements OnChanges, AfterViewInit {

  @Input('appErrors')
  public passedControl: FormControl;

  public subject = new BehaviorSubject<ErrorDetails>(null);

  public control: AbstractControl;

  @Output() public hasError = new EventEmitter<boolean>();

  public ready: boolean = false;

  private subscription: Subscription;

  constructor(private form: FormGroupDirective) {
  }

  public get errors() {
    if (!this.ready) {
      return;
    }
    return this.control.errors;
  }

  // public get hasErrors() {
  //   return !!this.errors;
  // }

  // public hasError(name: string, conditions: ErrorOptions): boolean {
  //   return this.checkPropState('invalid', name, conditions);
  // }

  // public isValid(name: string, conditions: ErrorOptions): boolean {
  //   return this.checkPropState('valid', name, conditions);
  // }

  public getError(name: string) {
    if (!this.ready) {
      return;
    }
    return this.control.getError(name);
  }

  public ngOnChanges() {
    this.control = this.passedControl;
  }

  public ngAfterViewInit() {
    this.checkStatus();
    this.subscription = this.control.statusChanges.subscribe(this.checkStatus.bind(this));
  }

  // private checkPropState(prop: string, name: string, conditions: ErrorOptions): boolean {
  //   if (!this.ready) {
  //     return;
  //   }
  //   const controlPropsState = (
  //     !conditions || toArray(conditions).every((condition: string) => this.control[condition])
  //   );
  //   if (name.charAt(0) === '*') {
  //     return this.control[prop] && controlPropsState;
  //   }
  //   return (
  //     prop === 'valid' ? !this.control.hasError(name) : this.control.hasError(name) && controlPropsState
  //   );
  // }

  private checkStatus() {
    const control = this.control;
    const errors = control.errors;
    this.ready = true;
    if (!errors) {
      return;
    }
    for (const errorName in errors) {
      if (errors.hasOwnProperty(errorName)) {
        this.subject.next({control, errorName, errorValue: errors[errorName]});
      }
    }
  }

}
