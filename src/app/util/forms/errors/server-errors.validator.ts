import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { ValidationErrors } from '@angular/forms/src/directives/validators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

export const ServerErrorsValidator = (errorsState): AsyncValidatorFn | null => {
  return (control: AbstractControl): Observable<ValidationErrors> => {
    return errorsState.asObservable().map((val) => {
      console.log(val);
      return val ? {serverError: {...val}} : null;
    });
  };
};
