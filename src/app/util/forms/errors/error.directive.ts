import {
  Directive, Input, HostBinding,
  Host, AfterViewInit, ViewContainerRef,
} from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';

import { ErrorOptions } from './errors.interface';
import { toArray } from './toArray';
import { ErrorsDirective } from './errors.directive';
import { InputContainerComponent } from '../input-container/input-container.component';
import { AutoUnsubscribe } from '../../helpers/decorators';
import { ServerErrorsValidator } from './server-errors.validator';
import * as _ from 'lodash';

//tslint:disable

@AutoUnsubscribe()
@Directive({
  selector: 'app-error'
})
export class ErrorDirective implements AfterViewInit {

  @Input()
  public set key(value: ErrorOptions) {
    this.errorNames = toArray(value);
  }

  @Input()
  public serverErrorState;


  @HostBinding('hidden')
  public hidden: boolean = true;


  public errorNames: string[];

  public subscription: Subscription;

  private errorsList: ErrorsDirective;

  constructor(@Host() private container: InputContainerComponent,
              private viewContainer: ViewContainerRef) {
  }

  public ngAfterViewInit() {
    this.errorsList = this.container.errorsDirective;
    if (this.serverErrorState) {
      this.errorsList.control.setAsyncValidators(ServerErrorsValidator(this.serverErrorState));
      this.errorNames.unshift('serverError')
    }

    const errors = this.errorsList.subject
      .filter(Boolean)
      .filter((obj) => !!~this.errorNames.indexOf(obj.errorName));

    const changes = this.errorsList.control.valueChanges;


    this.subscription = Observable.combineLatest(errors, changes)
      .mergeMap(([errs]) => {
        if (errs.control.hasError('serverError')) {
          const errorMessage = this.errorNames.length > 1 ? [...this.errorNames.slice(1)] : [...this.errorNames];
          return Observable.of(errs.errorValue)
            .pluck(...errorMessage)
            .map((v) => {
              if (v) {
                this.viewContainer.element.nativeElement.innerHTML = v;
              }
              this.removeServerError(errs);
              return this.hidden = !v;
            })
        } else {
          return Observable.of(this.hidden = !errs.control.hasError(errs.errorName));
        }
      })
      .subscribe((val) => {
        this.errorsList.hasError.emit(!val);
      });
  }

  private removeServerError(errs) {
    if (_.has(errs.control.errors, this.errorNames.join('.'))) {
      _.set(errs.control.errors, this.errorNames.join('.'), null)
    }
  }
}
