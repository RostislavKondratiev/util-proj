import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DynamicFormService } from '../dynamic-form.service';
import { DynamicFormConfig, DynamicFormItem } from '../dynamic-form.entities';

@Component({
  selector: 'app-dynamic-group',
  templateUrl: './dynamic-group.component.html',
  styleUrls: ['./dynamic-group.component.scss']
})
export class DynamicGroupComponent {
  @Input() public config: DynamicFormItem[];
  @Input() public group: FormGroup;
  @Input() public serverErrorState;

  constructor(private dynamicService: DynamicFormService) {
  }
}
