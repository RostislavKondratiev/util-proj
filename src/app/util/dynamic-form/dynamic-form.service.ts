import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  CreatedForm, DynamicFormConfig, DynamicFormControl, DynamicFormGroup, DynamicFormItem,
  FormKinds
} from './dynamic-form.entities';
import { AppValidators } from '../../helpers/appValidators';

@Injectable()
export class DynamicFormService {

  public createForm(config: DynamicFormItem[]): CreatedForm {
    const group = new FormGroup({});
    this.createGroup(config, group);
    return {group, config};
  }

  private createGroup(config, group, path = []) {
    config.forEach((item) => {
      switch (item.kind) {
        case FormKinds.Control:
          this.dynamicControl(group, item, path);
          break;
        case FormKinds.Group:
          this.dynamicGroup(group, item, path);
          break;
        default:
          break;
      }
    });
  }

  private dynamicControl(group, item, path = []) {
    item.path = [...path, item.name];
    const validators = [];
    if (item.validators) {
      item.validators.forEach((i) => {
        const validatorKey = Object.keys(i)[0];
        validators.push(AppValidators[validatorKey](i[validatorKey]));
      });
    }
    group.addControl(item.name,
      new FormControl(null, item.isRequired ? [Validators.required] : null));
  }

  private dynamicGroup(group, item, path = []) {
    const enclosedGroup = new FormGroup({});
    group.addControl(item.name, enclosedGroup);
    this.createGroup(item.controls, enclosedGroup, [...path, item.name]);
  }
}
