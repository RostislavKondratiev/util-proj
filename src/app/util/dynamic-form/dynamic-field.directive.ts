import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnInit, ViewContainerRef } from '@angular/core';
import { DynamicGroupComponent } from './dynamic-group/dynamic-group.component';
import { DynamicTextInputComponent } from './dynamic-text-input/dynamic-text-input.component';
import { FormGroup } from '@angular/forms';
import { FormKinds } from './dynamic-form.entities';

const components = {
  group: DynamicGroupComponent,
  text: DynamicTextInputComponent
};

@Directive({
  selector: '[appDynamicField]'
})
export class DynamicFieldDirective implements OnInit {
  @Input() public config;
  @Input() public group: FormGroup;
  @Input() public serverErrorState;

  public component: ComponentRef<any>;

  constructor(private resolver: ComponentFactoryResolver,
              private container: ViewContainerRef) {
  }

  public ngOnInit() {
    switch (this.config.kind) {
      case FormKinds.Control:
        const control = this.resolver.resolveComponentFactory<any>(components[this.config.type]);
        this.component = this.container.createComponent(control);
        this.component.instance.config = this.config;
        this.component.instance.group = this.group;
        this.component.instance.serverErrorState = this.serverErrorState;
        break;
      case FormKinds.Group:
        const group = this.resolver.resolveComponentFactory<any>(components[this.config.kind]);
        this.component = this.container.createComponent(group);
        this.component.instance.config = this.config.controls;
        this.component.instance.group = this.group.get(this.config.name);
        this.component.instance.serverErrorState = this.serverErrorState;
        break;
      default:
        break;
    }
  }

}
