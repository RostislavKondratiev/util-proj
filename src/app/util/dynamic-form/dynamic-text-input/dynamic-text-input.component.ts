import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dynamic-text',
  templateUrl: './dynamic-text-input.component.html',
  styleUrls: ['./dynamic-text-input.component.scss']
})
export class DynamicTextInputComponent {
  @Input() public config;
  @Input() public group: FormGroup;
  @Input() public serverErrorState;
}
