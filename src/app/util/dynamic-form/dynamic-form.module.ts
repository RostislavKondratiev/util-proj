import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFormsModule } from '../forms/app-forms.module';
import { DynamicGroupComponent } from './dynamic-group/dynamic-group.component';
import { DynamicTextInputComponent } from './dynamic-text-input/dynamic-text-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicFormService } from './dynamic-form.service';
import { DynamicFieldDirective } from './dynamic-field.directive';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';

const COMPONENTS = [
];

const ENTRY_COMPONENTS = [
  DynamicGroupComponent,
  DynamicTextInputComponent
];

const DIRECTIVES = [
  DynamicFieldDirective
];

@NgModule({
  imports: [
    CommonModule,
    AppFormsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    ...COMPONENTS,
    ...ENTRY_COMPONENTS,
    ...DIRECTIVES
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS
  ],
  providers: [
    DynamicFormService,
  ],
  exports: [
    ...COMPONENTS,
    ...ENTRY_COMPONENTS,
    ...DIRECTIVES
  ]
})
export class DynamicFormModule {

}
