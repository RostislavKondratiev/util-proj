import { FormGroup } from '@angular/forms';

export enum FormKinds {
  Control = 'control',
  Group = 'group'
}

export interface CreatedForm {
  group: FormGroup;
  config: DynamicFormItem[];
}

export interface DynamicFormConfig {
  [key: string]: DynamicFormItem;
}

export interface DynamicFormControl {
  kind: string;
  type: string;
  name: string;
  label: string;
  path: string[];
  isRequired: Boolean;
}

export interface DynamicFormGroup {
  kind: string;
  label: string;
  name: string;
  path: string[];
  controls: DynamicFormControl[];
}

export interface DynamicFormItem {
  kind: string;
  type: string;
  name: string;
  label: string;
  path: string[];
  isRequired?: Boolean;
  controls?: DynamicFormControl[];
}
