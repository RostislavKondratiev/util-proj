import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoodsComponent } from './goods.component';
import { NewGoodComponent } from './new-good/new-good.component';
import { NewGoodDynamicComponent } from './new-good-dynamic/new-good-dynamic.component';

const ROUTES: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'add'},
  {path: 'add', component: NewGoodComponent},
  {path: 'add-dynamic', component: NewGoodDynamicComponent}
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class GoodsRoutingModule {}
