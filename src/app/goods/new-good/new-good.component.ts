import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppValidators } from '../../helpers/appValidators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/publish';

@Component({
  selector: 'app-new-good',
  templateUrl: './new-good.component.html',
  styleUrls: ['./new-good.component.scss']
})
export class NewGoodComponent implements OnInit {
  public ngModel = false;
  public error = null;
  public errorsState$ = new BehaviorSubject<any>(null);
  public goodForm = new FormGroup({
    title: new FormControl(null, [AppValidators.required, AppValidators.maxLength(2)]),
    age: new FormControl(null, [AppValidators.required, AppValidators.adult(18)]),
    type: new FormControl(null, [AppValidators.required]),
    price: new FormControl(2, [AppValidators.required]),
    description: new FormControl(null, [AppValidators.required]),
    active: new FormControl(null, [AppValidators.requiredTrue]),
    seller: new FormGroup({
      first: new FormControl(null, [AppValidators.required]),
      last: new FormControl(null, [AppValidators.required]),
      address: new FormGroup({
        city: new FormControl(null, [AppValidators.required]),
        zip: new FormControl(null, [AppValidators.required])
      })
    })
  });

  public submitForm($event) {
    $event.preventDefault();
    console.log(this.goodForm.value);
    this.errorsState$.next(this.error = {
        title: 'This title is already taken',
        age: 'Age error',
        seller: {
          address: {
            city: 'Invalid City',
          }
        }
    });

  }

  public cleanErrors() {
    this.errorsState$.next(null);
  }

  public ngOnInit() {
    // setTimeout(() => {
    //   this.errorsState$.next(this.error = {
    //     title: 'This title is already taken'
    //   });
    // }, 3000);
  }
}
