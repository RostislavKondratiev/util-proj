import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFormsModule } from '../util/forms/app-forms.module';
import { NewGoodComponent } from './new-good/new-good.component';
import { GoodsComponent } from './goods.component';
import { GoodsRoutingModule } from './goods-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewGoodDynamicComponent } from './new-good-dynamic/new-good-dynamic.component';
import { NewGoodDynamicService } from './new-good-dynamic/new-good-dynamic.service';
import { DynamicFormModule } from '../util/dynamic-form/dynamic-form.module';

@NgModule({
  imports: [
    CommonModule,
    GoodsRoutingModule,
    AppFormsModule,
    DynamicFormModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    NewGoodComponent,
    NewGoodDynamicComponent,
    GoodsComponent,
  ],
  providers: [
    NewGoodDynamicService
  ],
  exports: []
})
export class GoodsModule {
}
