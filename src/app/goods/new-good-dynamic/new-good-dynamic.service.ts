import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DynamicFormConfig, DynamicFormItem } from '../../util/dynamic-form/dynamic-form.entities';

@Injectable()
export class NewGoodDynamicService {
  constructor(private http: HttpClient) {
  }

  public getDynamicForm() {
    return this.http.get<DynamicFormItem[]>('http://localhost:3000/addForm');
  }
}
