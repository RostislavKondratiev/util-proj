import { Component, OnInit } from '@angular/core';
import { NewGoodDynamicService } from './new-good-dynamic.service';
import { DynamicFormItem } from '../../util/dynamic-form/dynamic-form.entities';
import { DynamicFormService } from '../../util/dynamic-form/dynamic-form.service';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-new-good-dynamic',
  templateUrl: './new-good-dynamic.component.html',
  styleUrls: ['./new-good-dynamic.component.scss']
})
export class NewGoodDynamicComponent implements OnInit {
  public formConfig: DynamicFormItem[];
  public rootGroup: FormGroup = null;
  public serverErrorState$ = new BehaviorSubject<any>(null);

  constructor(private service: NewGoodDynamicService,
              private dynamicService: DynamicFormService) {
  }

  public ngOnInit() {
    this.service.getDynamicForm().subscribe((res) => {
      const form = this.dynamicService.createForm(res);
      this.formConfig = form.config;
      this.rootGroup = form.group;
      console.log(this.rootGroup);
      this.rootGroup.valueChanges.subscribe((val) => {
        console.log(val);
      });
    });
  }

  public submitForm($event) {
    console.log('submitted');
    console.log(this.rootGroup);
    this.serverErrorState$.next({
      first_name: 'This title is already taken',
      age: 'Age error',
      address: {
        city: 'Invalid City',
        contacts: {
          email: 'Invalid Email'
        }
      }
    });
  }
}
