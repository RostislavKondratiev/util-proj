import { AbstractControl, Validators } from '@angular/forms';
import { ValidationErrors } from '@angular/forms/src/directives/validators';

export class AppValidators extends Validators {
  public static adult(target: number) {
    return (control: AbstractControl) : ValidationErrors | null => {
      if (control.value && control.value < target) {
        return { adult: true };
      } else {
        return null;
      }
    };
  }
  constructor() {
    super();
  }
}
